# Oficina de Projetos - Grupo 6

Criação da infraestrutura inicial para a implantação do [Mautic][mautic] na AWS.

Por enquanto cria apenas a VPC e os Security Groups

## Passos necessários

1. Instalar AWS CLI e configurar as credenciais
2. Instalar o Terraform
3. Definir a região no arquivo variables.tf

## Comandos do Terraform

Inicializar 
```bash
terraform init
```

Verificar os recursos que serão criados
```bash
terraform plan
```

Aplicar as configurações
```bash
terraform apply
```

Destruir o ambiente
```bash
terraform destroy
```

[mautic]: https://www.mautic.org
